# Koodikoulu / Bunny Awakens

To start application run `npm install && npm start`

# Building

To build application in production mode `npm run build`

Compiling with webpack analysing enabled `npm run compile:profile`
Resulsts are saved to file `webpack.profile.json` which can be opened with http://webpack.github.io/analyse/

# TODO

- https://www.npmjs.com/package/circular-dependency-plugin
- Generate test coverage report
- Create sort & lint script for localization files
- Create script for removing unused localization strings (https://www.npmjs.com/package/find-in-files)
- Store application data by using `Cache API` (https://developers.google.com/web/fundamentals/instant-and-offline/web-storage/cache-api)
- Add neat css tricks! https://developer.mozilla.org/en-US/docs/Web/CSS/filter
- Whuuu! http://thenewcode.com/540/Animating-CSS-Image-Filters
- Add typeform to get some feedback: http://typeform.io

# Evaluating typescript for game code

This actually works, but we can't get any feedback which command is currently executed.

```typescript
import * as ts from 'typescript'

tslint:disable-next-line
let windowInstance: any = window
windowInstance.player = {
  hoplaa() {
    // tslint:disable-next-line
    console.log('wou!')
  }
}

const bunnyCode = `
  const bunny = {
    action: () => {
      // tslint:disable-next-line
      console.log('WAY OF BUNNIES')
      window.player.hoplaa()
    }
  }
`

const fullCode = `
  ${bunnyCode}
  ${code}
`

const result = ts.transpile(fullCode)
logger.info('result', result)

// tslint:disable-next-line
const runnable = eval(`({
  run: () => {
    ${result}
  }
})`)

runnable.run()

// in game window run script: `bunny.action()`
```
