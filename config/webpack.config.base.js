const helpers = require('./helpers')
const NamedModulesPlugin = require('webpack/lib/NamedModulesPlugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const vue = helpers.root('node_modules/vue/dist/vue.esm.js')
const phaser = helpers.root('node_modules/phaser-ce/build/custom/phaser-split.js')
const pixi = helpers.root('node_modules/phaser-ce/build/custom/pixi.js')
const p2 = helpers.root('node_modules/phaser-ce/build/custom/p2.js')

let webpackConfig = {
  context: helpers.root('./src/client'),
  entry: {
    main: './main.ts',
    vendor: ['pixi', 'p2', 'phaser']
  },
  output: {
    path: helpers.root('/dist'),
    filename: 'js/[name].[hash].js',
    chunkFilename: 'js/[name].[hash].js',
    publicPath: '/'
  },
  resolve: {
    extensions: [ '.ts', '.js', '.html', '.vue' ],
    alias: {
      'vue$': vue,
      'phaser': phaser,
      'pixi': pixi,
      'p2': p2
    }
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        enforce: 'pre',
        use: [
          { loader: 'tslint-loader' }
        ]
      },
      {
        test: /\.ts$/,
        use: [
          { loader: 'ts-loader' }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.vue$/,
        use: [
          { loader: 'vue-loader' }
        ]
      },
      {
        test: /\.html$/,
        use: [
          { loader: 'raw-loader' }
        ],
        exclude: ['./index.html']
      },
      {
        test: pixi,
        use: [{
          loader: 'expose-loader',
          options: 'PIXI'
        }]
      },
      {
        test: phaser,
        use: [{
          loader: 'expose-loader',
          options: 'Phaser'
        }]
      },
      {
        test: p2,
        use: [{
          loader: 'expose-loader',
          options: 'p2'
        }]
      }
    ]
  },
  plugins: [
    new NamedModulesPlugin(),
    new CopyWebpackPlugin([{
      from: './assets',
      to: './assets'
    }])
  ]
}

module.exports = webpackConfig
