const path = require('path')
const helpers = require('./helpers')
const NamedModulesPlugin = require('webpack/lib/NamedModulesPlugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const DefinePlugin = require('webpack/lib/DefinePlugin')
const env = require('../environment/dev.env')

const vue = helpers.root('node_modules/vue/dist/vue.esm.js')
const phaser = helpers.root('node_modules/phaser-ce/build/custom/phaser-split.js')
const pixi = helpers.root('node_modules/phaser-ce/build/custom/pixi.js')
const p2 = helpers.root('node_modules/phaser-ce/build/custom/p2.js')

const webpackConfig = {
  node: {
    __dirname: false
  },
  context: helpers.root('./src/client'),
  entry: {
    main: './main.ts',
    vendor: ['pixi', 'p2', 'phaser']
  },
  output: {
    path: helpers.root('/dist'),
    filename: 'js/[name].[hash].js',
    chunkFilename: 'js/[name].[hash].js',
    publicPath: '/'
  },
  // devtool: 'eval-source-map'
  resolve: {
    extensions: [ '.ts', '.js', '.html', '.vue' ],
    alias: {
      'vue$': vue,
      'phaser': phaser,
      'pixi': pixi,
      'p2': p2
    }
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        enforce: 'pre',
        use: [ { loader: 'tslint-loader' } ]
      },
      {
        test: /\.ts$/,
        use: [ { loader: 'ts-loader' } ],
        exclude: /node_modules/
      },
      {
        test: /\.vue$/,
        use: [ { loader: 'vue-loader' } ],
        exclude: /node_modules\/(?!(bootstrap-vue)\/).*/
      },
      {
        test: /\.html$/,
        use: [ { loader: 'raw-loader' } ],
        exclude: [
          /node_modules/,
          './index.html'
        ]
      },
      {
        test: pixi,
        use: [{
          loader: 'expose-loader',
          options: 'PIXI'
        }]
      },
      {
        test: phaser,
        use: [{
          loader: 'expose-loader',
          options: 'Phaser'
        }]
      },
      {
        test: p2,
        use: [{
          loader: 'expose-loader',
          options: 'p2'
        }]
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' }
        ]
      },
      {
        test: /\.(jpg|png|gif|eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      filename: './index.html',
      template: './index.html',
      favicon: './assets/favicon.ico'
    }),
    new DefinePlugin({
      'process.env': env
    }),
    new NamedModulesPlugin()
  ],
  devServer: {
    port: 8080,
    host: 'localhost',
    historyApiFallback: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    },
    contentBase: './src/client',
    open: false
  }
}

module.exports = webpackConfig
