const path = require('path')
const glob = require('glob')
const autoprefixer = require('autoprefixer')

const CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin')
const CompressionPlugin = require('compression-webpack-plugin')
const DefinePlugin = require('webpack/lib/DefinePlugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const PurifyCSSPlugin = require('purifycss-webpack')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OfflinePlugin = require('offline-plugin')


const webpackConfig = require('./webpack.config.base')
const env = require('../environment/prod.env')
const helpers = require('./helpers')

const extractSass = new ExtractTextPlugin({
  filename: 'css/[name].[contenthash].css',
  disable: process.env.NODE_ENV === 'development'
})

const purifyCss = new PurifyCSSPlugin({
  paths: glob.sync(path.join(__dirname, '../src/client/**/*.html')),
  purifyOptions: {
    info: true,
    whitelist: []
  }
})

webpackConfig.module.rules = [...webpackConfig.module.rules,
  {
    test: /\.css$/,
    use: [
      { loader: 'style-loader' },
      { loader: 'css-loader' },
      { loader: 'sass-loader' }
]
  },
  {
    test: /\.scss$/,
    use: extractSass.extract({
      use: [
        {
          loader: 'css-loader',
          options: {
            minimize: true,
            sourceMap: true,
            importLoaders: 2
          }
        },
        {
          loader: 'postcss-loader',
          options: {
            ident: 'postcss',
            plugins: () => [autoprefixer]
          }
        },
        {
          loader: 'sass-loader',
          options: {
            outputStyle: 'expanded',
            sourceMap: true,
            sourceMapContents: true
          }
        }
      ],
      fallback: 'style-loader'
    })
  },
  {
    test: /\.(jpg|png|gif)$/,
    use: [
      { loader: 'file-loader?name=assets/img/[name].[ext]' }
    ]
  },
  {
    test: /\.(eot|svg|ttf|woff|woff2)$/,
    use: [
      { loader: 'file-loader?name=fonts/[name].[ext]' }
    ]
  }
]

// // ensure ts lint fails the build
// webpackConfig.module.rules[0].use[0].options = {
//   failOnHint: true
// }

webpackConfig.plugins = [...webpackConfig.plugins,
  new CommonsChunkPlugin({
    name: 'vendor',
    minChunks: function(module){
      return module.context && module.context.indexOf('node_modules') !== -1;
    }
  }),
  new CommonsChunkPlugin({
    name: 'manifest',
    minChunks: Infinity
  }),
  extractSass,
  // purifyCss,
  new HtmlWebpackPlugin({
    inject: true,
    filename: helpers.root('/src/client/index.html'),
    template: helpers.root('/src/client/index.html'),
    favicon: helpers.root('/src/client/assets/favicon.ico'),
    minify: {
      removeComments: true,
      collapseWhitespace: true,
      removeRedundantAttributes: true,
      useShortDoctype: true,
      removeEmptyAttributes: true,
      removeStyleLinkTypeAttributes: true,
      keepClosingSlash: true,
      minifyJS: true,
      minifyCSS: true,
      minifyURLs: true
    }
  }),
  new UglifyJsPlugin(),
  new CompressionPlugin({
    asset: '[path].gz[query]',
    test: /\.js$/
  }),
  new DefinePlugin({
    'process.env': env
  }),
  new FaviconsWebpackPlugin(helpers.root('/src/client/assets/icon.png')),
  new OfflinePlugin()
]

webpackConfig.devtool = 'source-map'

module.exports = webpackConfig
