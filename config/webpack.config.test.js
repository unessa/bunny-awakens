const helpers = require('./helpers')
const DefinePlugin = require('webpack/lib/DefinePlugin')
const env = require('../environment/dev.env')

const vue = helpers.root('node_modules/vue/dist/vue.esm.js')
const phaser = helpers.root('node_modules/phaser-ce/build/custom/phaser-split.js')
const pixi = helpers.root('node_modules/phaser-ce/build/custom/pixi.js')
const p2 = helpers.root('node_modules/phaser-ce/build/custom/p2.js')

const webpackConfig = {
  resolve: {
    extensions: [ '.ts', '.js', '.html', '.vue' ],
    alias: {
      'vue$': vue,
      'phaser': phaser,
      'pixi': pixi,
      'p2': p2
    }
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['env'],
              plugins: [
                'transform-runtime',
                'array-includes'
              ],
              cacheDirectory: true
            }
          },
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true
           }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.vue$/,
        use: [ { loader: 'vue-loader' } ],
        exclude: /node_modules\/(?!(bootstrap-vue)\/).*/
      },
      {
        test: /\.html$/,
        use: [ { loader: 'raw-loader' } ],
        exclude: [
          /node_modules/,
          './src/index.html'
        ]
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' }
        ]
      },
      {
        test: pixi,
        use: [{
          loader: 'expose-loader',
          options: 'PIXI'
        }]
      },
      {
        test: phaser,
        use: [{
          loader: 'expose-loader',
          options: 'Phaser'
        }]
      },
      {
        test: p2,
        use: [{
          loader: 'expose-loader',
          options: 'p2'
        }]
      }
    ]
  },
  plugins: [
    new DefinePlugin({
      'process.env': env
    })
  ]
}

module.exports = webpackConfig
