var webpackConfig = require('./config/webpack.config.test');

module.exports = function(config) {
  config.set({
    basePath: '.',
    frameworks: ['jasmine'],
    files: [
      'node_modules/es6-promise/dist/es6-promise.auto.js',
      'src/client/specs.ts'
    ],
    preprocessors: {
      'src/client/specs.ts': ['webpack']
    },
    webpack: webpackConfig,
    webpackServer: { noInfo: true },
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: true,
    mime: {
      'text/x-typescript': ['ts']
    }
  })
}
