import 'pixi'
import 'p2'
import * as Phaser from 'phaser-ce'
import logger from '../util/logger'
import { BootState } from './state/boot-state'
import { GameState } from './state/game-state'
import { GAME_STATE_BOOT, GAME_STATE_GAME } from './constants'
import { IGameEngineCommand } from '../game-code'
import { GameExecutionCycle, IBunnyGameState, StatusChangedCallback } from './game-execution-cycle'

export class BunnyGame extends Phaser.Game {
  private internalState: IBunnyGameState
  private gameCycle: GameExecutionCycle

  constructor(
    // id-element name where game is rendered
    gameElementId: string,
    // The width of the viewport
    width: number,
    // The height of the viewport
    height: number,
    // This event is called once per game's cycle.
    // Game cycle is consideded to be one command execution lifetime (ie. bunny moves right)
    onStatusChanged: StatusChangedCallback
  ) {
    logger.info(`Creating game: ${gameElementId}, ${width}x${height}`)
    super(width, height, Phaser.AUTO, gameElementId, null)

    this.state.add(GAME_STATE_BOOT, BootState, false)
    this.state.add(GAME_STATE_GAME, GameState, false)
    this.state.start(GAME_STATE_BOOT)

    this.gameCycle = new GameExecutionCycle(
      this.state.states[GAME_STATE_GAME],
      onStatusChanged
    )
  }

  public setAndRunGameCode(gameCommands: IGameEngineCommand[]): void {
    this.gameCycle.executeCommands(gameCommands)
  }

  public changeSpeed(): void {
    logger.info('Game::changeSpeed')
  }
}
