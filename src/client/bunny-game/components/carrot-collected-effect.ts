import 'pixi'
import 'p2'
import * as Phaser from 'phaser-ce'
import { ASSET_CARROT } from '../constants'

const NUMBER_OF_PARTICLES = 10
const PARTICLE_LIFE_TIME = 3000

export class CarrotCollectedEffect {
  private emitter: Phaser.Particles.Arcade.Emitter

  constructor(private game: Phaser.Game) {
    this.emitter = this.game.add.emitter()
    this.emitter.makeParticles(ASSET_CARROT)
    this.emitter.gravity.y = 200
    this.emitter.setAlpha(1, 0.1, PARTICLE_LIFE_TIME)
    this.emitter.setScale(1, 0, 1, 0, PARTICLE_LIFE_TIME, Phaser.Easing.Quintic.Out)
  }

  public destroy(): void {
    this.emitter.destroy()
  }

  public start(carrot: Phaser.Sprite): void {
    this.emitter.x = carrot.x
    this.emitter.y = carrot.y
    this.emitter.start(true, PARTICLE_LIFE_TIME, undefined, NUMBER_OF_PARTICLES)
  }
}
