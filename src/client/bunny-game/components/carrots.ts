import 'pixi'
import 'p2'
import * as Phaser from 'phaser-ce'
import { CarrotCollectedEffect } from './carrot-collected-effect'
import { ASSET_CARROT, LAYER_CARROTS } from '../constants'
import { Player } from './player'

export class Carrots {
  private carrots: Phaser.Group
  private carrotCollectedEffect: CarrotCollectedEffect

  constructor(
    private phaserGame: Phaser.Game,
    private tilemap: Phaser.Tilemap
  ) {
    this.carrots = this.phaserGame.add.group()
    this.tilemap.createFromObjects(LAYER_CARROTS, 1, ASSET_CARROT, 0, true, false, this.carrots)
    this.carrotCollectedEffect = new CarrotCollectedEffect(this.phaserGame)

    this.carrots.forEach((carrot: Phaser.Sprite) => {
      carrot.events.onKilled.addOnce(() => this.carrotCollectedEffect.start(carrot))
    }, this)
  }

  public destroy(): void {
    if (this.carrots) {
      this.carrots.destroy()
    }

    if (this.carrotCollectedEffect) {
      this.carrotCollectedEffect.destroy()
    }
  }

  public update(player: Player): void {
    this.carrots.forEachAlive(this.collectCarrot, this, player)
  }

  private collectCarrot(
    carrot: Phaser.Sprite,
    player: Player
  ): void {
    if (player.collidesWith(carrot)) {
      carrot.kill()
    }
  }

  public hasAllCarrotsCollected(): boolean {
    return this.carrots.countLiving() === 0
  }

  public getNumberOfCollectedCarrots(): number {
    return this.carrots.countDead()
  }

  public getTotalNumberOfCarrots(): number {
    return this.carrots.length
  }
}
