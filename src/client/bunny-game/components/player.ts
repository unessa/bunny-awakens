import 'pixi'
import 'p2'
import * as Phaser from 'phaser-ce'
import { LAYER_COLLISION } from '../constants'
import logger from '../../util/logger'

const PLAYER_ANIMATION_DURATION = 500

export class Player {
  private player: Phaser.Sprite
  private currentTile: Phaser.Tile

  constructor(
    private game: Phaser.Game,
    private tilemap: Phaser.Tilemap,
    assetName: string,
    x: number = 0,
    y: number = 0
  ) {
    this.player = this.game.add.sprite(x, y, assetName)
    this.currentTile = this.tilemap.getTileWorldXY(x, y)
  }

  public destroy(): void {
    this.player.destroy()
  }

  public movePlayer(
    tileMovementX: number,
    tileMovementY: number,
    onCompleted: () => void
  ): void {
    const nextTile = this.getNextPlayerTile(tileMovementX, tileMovementY)
    const hasCollision = this.checkTilemapCollision(nextTile.x, nextTile.y)

    if (!hasCollision) {
      this.currentTile = nextTile

      const tween = this.createMoveTween(nextTile)
      tween.onComplete.addOnce(onCompleted, this)
      tween.start()
    } else {
      onCompleted()
    }
  }

  private getNextPlayerTile(tileX: number, tileY: number): Phaser.Tile {
    return this.tilemap.getTile(this.currentTile.x + tileX, this.currentTile.y + tileY)
  }

  private checkTilemapCollision(tileX: number, tileY: number): boolean {
    const tile = this.tilemap.getTile(tileX, tileY, LAYER_COLLISION)
    return tile && tile.index > 0
  }

  private createMoveTween(nextTile: Phaser.Tile): Phaser.Tween {
    return this.game.add.tween(this.player).to({
      x: nextTile.worldX,
      y: nextTile.worldY
    }, PLAYER_ANIMATION_DURATION, Phaser.Easing.Linear.name)
  }

  public collidesWith(sprite: Phaser.Sprite): boolean {
    const spriteTile = this.tilemap.getTileWorldXY(sprite.centerX, sprite.centerY)
    return spriteTile === this.currentTile
  }
}
