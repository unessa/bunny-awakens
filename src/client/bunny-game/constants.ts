export const GAME_STATE_BOOT = 'BootState'
export const GAME_STATE_GAME = 'GameSate'

export const ASSET_LEVEL = 'ASSET_LEVEL'
export const ASSET_TILESET = 'ASSET_TILESET'
export const ASSET_BUNNY = 'ASSET_BUNNY'
export const ASSET_CARROT = 'ASSET_CARROT'

// These should match names in tilemap files
export const LAYER_GROUND = 'ground'
export const LAYER_DECORATION = 'decoration'
export const LAYER_COLLISION = 'collision'
export const LAYER_CARROTS = 'carrots'
