import { GameExecutionCycle, IBunnyGameState } from './game-execution-cycle'
import { PlayerAPIFunction } from './state/game-state'

import {
  ISourceLocation,
  INVALID_LOCATION,
  COMMAND_MOVE_LEFT,
  COMMAND_MOVE_RIGHT,
  COMMAND_MOVE_UP,
  COMMAND_MOVE_DOWN
} from '../game-code'

const mockedPlayerAPIFunction: PlayerAPIFunction = (callback: () => void) => { callback() }

const mockedAPI = new Map<string, PlayerAPIFunction>()
mockedAPI.set(COMMAND_MOVE_LEFT, mockedPlayerAPIFunction)
mockedAPI.set(COMMAND_MOVE_RIGHT, mockedPlayerAPIFunction)
mockedAPI.set(COMMAND_MOVE_UP, mockedPlayerAPIFunction)
mockedAPI.set(COMMAND_MOVE_DOWN, mockedPlayerAPIFunction)

function createCommandAPISpy(
  api: Map<string, PlayerAPIFunction>
): jasmine.Spy {
  return spyOn(mockedAPI, 'get').and.callFake((name: string) => {
    return name
  }).and.callThrough()
}

const mockedGameState: any = {
  getTotalNumberOfCarrots: (): number => {
    return 3
  },
  getNumberOfCollectedCarrots: (): number => {
    return 0
  },
  hasAllCarrotsCollected: (): boolean => {
    return false
  },
  GameEngineAPI: () => {
    return mockedAPI
  }
}

const gameCycle = new GameExecutionCycle(
  mockedGameState,
  (state: IBunnyGameState) => { return }
)

const MOCKED_LOCATION: ISourceLocation = {
  start: INVALID_LOCATION,
  end: INVALID_LOCATION
}

let spy: jasmine.Spy

describe(`Bunny Game Cycle`, () => {
  it(`should throw an error if command is not found`, () => {
    const undefinedCommand = 'undefinedCommand'
    const throws = () => gameCycle.executeCommands([{
      name: undefinedCommand,
      arguments: [],
      location: MOCKED_LOCATION
    }])
    expect(throws).toThrowError(`Invalid command: ${undefinedCommand}`)
  })

  describe(`Calling through all available commands`, () => {
    beforeAll(() => spy = createCommandAPISpy(mockedAPI))
    mockedAPI.forEach((value, key) => {
      beforeAll(() => {
        gameCycle.resetGameState()
        gameCycle.executeCommands([{
          name: key,
          arguments: [],
          location: MOCKED_LOCATION
        }])
      })

      it(`should call function: ${key}`, () => {
        expect(spy).toHaveBeenCalledWith(key)
      })
    })
  })

  describe(`Calling commands in sequence`, () => {
    beforeAll(() => {
      spy = createCommandAPISpy(mockedAPI)
      gameCycle.resetGameState()
      gameCycle.executeCommands([
        { name: COMMAND_MOVE_LEFT, arguments: [], location: MOCKED_LOCATION },
        { name: COMMAND_MOVE_UP, arguments: [], location: MOCKED_LOCATION },
        { name: COMMAND_MOVE_LEFT, arguments: [], location: MOCKED_LOCATION },
        { name: COMMAND_MOVE_LEFT, arguments: [], location: MOCKED_LOCATION },
        { name: COMMAND_MOVE_UP, arguments: [], location: MOCKED_LOCATION },
        { name: COMMAND_MOVE_DOWN, arguments: [], location: MOCKED_LOCATION }
      ])
    })

    it(`should have runned commands in correct sequence`, () => {
      const calledCommands = Array.prototype.concat(...spy.calls.allArgs())
      expect(calledCommands).toEqual([
        COMMAND_MOVE_LEFT,
        COMMAND_MOVE_UP,
        COMMAND_MOVE_LEFT,
        COMMAND_MOVE_LEFT,
        COMMAND_MOVE_UP,
        COMMAND_MOVE_DOWN
      ])
    })
  })
})
