import { IGameEngineCommand } from '../game-code'
import { GameState } from './state/game-state'

export enum BunnyGameStatus {
  IDLE = 'IDLE',
  RUNNING = 'RUNNING',
  WIN = 'WIN',
  GAME_OVER = 'GAME_OVER',
  ERROR = 'ERROR'
}

export interface IBunnyGameState {
  status: BunnyGameStatus,
  gameSpeedFactor: number,
  currentExecutingCommand: IGameEngineCommand | null,
  carrotsCollected: number,
  totalCarrots: number,
}

export type StatusChangedCallback = (state: IBunnyGameState) => void

export class GameExecutionCycle {
  private bunnyGameState: IBunnyGameState
  private currentGameCommands: IGameEngineCommand[] | null = null
  private currentGameCommandIndex: number

  constructor(
    private phaserGameState: GameState,
    private onStatusChangedCallback: StatusChangedCallback
  ) {
    this.bunnyGameState = {
      status: BunnyGameStatus.IDLE,
      currentExecutingCommand: null,
      carrotsCollected: 0,
      totalCarrots: this.phaserGameState.getTotalNumberOfCarrots(),
      gameSpeedFactor: 1
    }
  }

  public resetGameState() {
    this.bunnyGameState.status = BunnyGameStatus.IDLE
    this.bunnyGameState.currentExecutingCommand = null
    this.bunnyGameState.carrotsCollected = 0
    this.bunnyGameState.totalCarrots = this.phaserGameState.getTotalNumberOfCarrots()
    this.onStatusChangedCallback(this.bunnyGameState)
  }

  public executeCommands(gameCommands: IGameEngineCommand[]): void {
    if (this.bunnyGameState.status !== BunnyGameStatus.IDLE) {
      throw new Error(`Unable to execute given commands. Current status is ${this.bunnyGameState.status}`)
    }

    this.currentGameCommands = gameCommands
    this.currentGameCommandIndex = -1

    this.bunnyGameState.status = BunnyGameStatus.RUNNING
    this.bunnyGameState.currentExecutingCommand = null
    this.bunnyGameState.carrotsCollected = this.phaserGameState.getNumberOfCollectedCarrots()
    this.bunnyGameState.totalCarrots = this.phaserGameState.getTotalNumberOfCarrots(),
    this.onStatusChangedCallback(this.bunnyGameState)

    this.executeNextCommand()
  }

  private executeNextCommand() {
    const currentCommand = this.getNextCommand()

    if (!currentCommand) {
      this.gameOver()
    } else {
      const commandFn = this.phaserGameState.GameEngineAPI().get(currentCommand.name)

      if (commandFn) {
        this.bunnyGameState.currentExecutingCommand = currentCommand
        this.onStatusChangedCallback(this.bunnyGameState)

        commandFn(() => this.executeGameCommandCallback())
      } else {
        throw new Error(`Invalid command: ${currentCommand.name}`)
      }
    }
  }

  private getNextCommand(): IGameEngineCommand | null {
    this.currentGameCommandIndex++
    return this.currentGameCommands && this.currentGameCommands[this.currentGameCommandIndex] || null
  }

  private gameOver(): void {
    this.bunnyGameState.status = BunnyGameStatus.GAME_OVER
    this.bunnyGameState.currentExecutingCommand = null
    this.bunnyGameState.carrotsCollected = this.phaserGameState.getNumberOfCollectedCarrots()
    this.onStatusChangedCallback(this.bunnyGameState)
  }

  private executeGameCommandCallback(): void {
    if (this.phaserGameState.hasAllCarrotsCollected()) {
      this.bunnyGameState.status = BunnyGameStatus.WIN
    }
    this.bunnyGameState.carrotsCollected = this.phaserGameState.getNumberOfCollectedCarrots()
    this.onStatusChangedCallback(this.bunnyGameState)

    if (!this.phaserGameState.hasAllCarrotsCollected()) {
      this.executeNextCommand()
    }
  }
}
