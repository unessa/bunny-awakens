import 'pixi'
import 'p2'
import * as Phaser from 'phaser-ce'
import logger from '../../util/logger'

import {
  GAME_STATE_GAME,
  ASSET_TILESET,
  ASSET_BUNNY,
  ASSET_CARROT
} from '../constants'

export class BootState extends Phaser.State {
  public preload() {
    logger.info('BootState::preload')

    this.load.image(ASSET_TILESET, 'assets/img/tilemap_basic_32x32.png')
    this.load.image(ASSET_BUNNY, 'assets/img/bunny_s.png')
    this.load.image(ASSET_CARROT, 'assets/img/carrot.png')
  }

  public create() {
    logger.info('BootSate::create')

    this.game.physics.startSystem(Phaser.Physics.ARCADE)

    this.stage.backgroundColor = '#787878'

    this.state.start(GAME_STATE_GAME, true, false, {
      levelNumber: 1
    })
  }
}
