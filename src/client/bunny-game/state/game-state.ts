import 'pixi'
import 'p2'
import * as Phaser from 'phaser-ce'
import logger from '../../util/logger'
import { Player } from '../components/player'
import { Carrots } from '../components/carrots'

import {
  COMMAND_MOVE_LEFT,
  COMMAND_MOVE_RIGHT,
  COMMAND_MOVE_UP,
  COMMAND_MOVE_DOWN
} from '../../game-code'

import {
  ASSET_LEVEL,
  ASSET_TILESET,
  ASSET_BUNNY,
  LAYER_GROUND,
  LAYER_DECORATION,
  LAYER_COLLISION
} from '../constants'

export type PlayerAPIFunction = (callback: () => void) => void

export class GameState extends Phaser.State {
  private levelNumber: number
  private tilemap: Phaser.Tilemap
  private tilemapLayers: Phaser.TilemapLayer[] = []
  private player: Player
  private carrots: Carrots

  public init(options: any) {
    this.levelNumber = options.levelNumber
    logger.info(`Start level ${this.levelNumber}`)
  }

  public preload(): void {
    // FIXME 21.12.2017 nviik - padStart comes from es2017 proposal. Figure out how to remove editor error notification
    const levelString = this.levelNumber.toString().padStart(3, '0')
    logger.info(`GameState::preload - load level tilemap: 'assets/levels/${levelString}.json'`)
    this.load.tilemap(ASSET_LEVEL, `assets/levels/${levelString}.json`, null, Phaser.Tilemap.TILED_JSON)
  }

  public create(): void {
    logger.info('GameState::create')

    this.createLevelTilemap()

    this.carrots = new Carrots(this.game, this.tilemap)
    this.player = new Player(this.game, this.tilemap, ASSET_BUNNY)
  }

  private createLevelTilemap(): void {
    this.tilemap = this.add.tilemap(ASSET_LEVEL)
    this.tilemap.addTilesetImage('tilemap_basic_32x32', ASSET_TILESET)

    const layerGround = this.tilemap.createLayer(LAYER_GROUND)
    layerGround.resizeWorld()
    this.tilemapLayers.push(layerGround)

    const layerDecoration = this.tilemap.createLayer(LAYER_DECORATION)
    layerDecoration.resizeWorld()
    this.tilemapLayers.push(layerDecoration)

    const layerCollision = this.tilemap.createLayer(LAYER_COLLISION)
    layerCollision.resizeWorld()
    this.tilemapLayers.push(layerCollision)
  }

  public shutdown(): void {
    logger.info('GameState::shutdown')

    if (this.player) {
      this.player.destroy()
    }

    if (this.tilemap) {
      this.tilemap.destroy()
    }

    while (this.tilemapLayers.length > 0) {
      const layer = this.tilemapLayers.pop()
      if (layer) {
        layer.destroy()
      }
    }

    if (this.carrots) {
      this.carrots.destroy()
    }
  }

  public update(): void {
    this.carrots.update(this.player)
  }

  public GameEngineAPI(): Map<string, PlayerAPIFunction> {
    const api = new Map<string, PlayerAPIFunction>()
    api.set(COMMAND_MOVE_LEFT, (cb: () => void) => this.player.movePlayer(-1, 0, cb))
    api.set(COMMAND_MOVE_RIGHT, (cb: () => void) => this.player.movePlayer(1, 0, cb))
    api.set(COMMAND_MOVE_UP, (cb: () => void) => this.player.movePlayer(0, -1, cb))
    api.set(COMMAND_MOVE_DOWN, (cb: () => void) => this.player.movePlayer(0, 1, cb))
    return api
  }

  public hasAllCarrotsCollected(): boolean {
    return this.carrots && this.carrots.hasAllCarrotsCollected()
  }

  public getNumberOfCollectedCarrots(): number {
    return this.carrots && this.carrots.getNumberOfCollectedCarrots() || 0
  }

  public getTotalNumberOfCarrots(): number {
    return this.carrots && this.carrots.getTotalNumberOfCarrots() || 0
  }
}
