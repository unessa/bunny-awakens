import { Component, Vue, Watch } from 'vue-property-decorator'
import * as ace from 'brace'
import logger from '../../util/logger'
import { Store, GameStateStatus } from '../../store'
import { IGameEngineCommand } from '../../game-code'
import 'brace/mode/javascript'
import 'brace/theme/dawn'
import './code-editor.scss'

const Range = ace.acequire('ace/range').Range
const EDITOR_ELEMENT_ID = 'code-editor'

@Component({
  template: require('./code-editor.html'),
})
export class CodeEditorComponent extends Vue {
  private editor: ace.Editor
  public isRunButtonDisabled: boolean = false
  public currentCommand: IGameEngineCommand
  private currentMarker: number

  mounted(): void {
    this.editor = ace.edit(EDITOR_ELEMENT_ID)
    this.editor.getSession().setMode('ace/mode/javascript')
    this.editor.setTheme('ace/theme/dawn')
    this.editor.setValue(this.getDevelopmentGameCode())
  }

  private getDevelopmentGameCode(): string {
    return `moveRight();
moveRight();
moveDown();
moveRight();
moveRight();
moveRight();
`
  }

  public executeCode(): void {
    const code = this.editor.getValue()
    Store.dispatch('executeCode', code)
  }

  @Watch('$store.state.game.status')
  private onGameStateStatusChanged(status: GameStateStatus): void {
    this.isRunButtonDisabled = (status === GameStateStatus.RUNNING)
    logger.info('GameStateStatus changed', this.isRunButtonDisabled)
  }

  @Watch('$store.state.game.currentCommand')
  private onCurrentCommandChanged(command: IGameEngineCommand): void {
    this.currentCommand = command

    const range = new Range(
      this.currentCommand.location.start.line - 1,
      this.currentCommand.location.start.column,
      this.currentCommand.location.end.line - 1,
      this.currentCommand.location.end.column
    )

    this.editor.getSession().removeMarker(this.currentMarker)
    this.currentMarker = this.editor.getSession().addMarker(range, 'highlight-line', 'fullLine', false)
  }
}
