import { ComponentTest } from '../../util/component-test'
import { GameActionsComponent } from './game-actions'

let component: GameActionsComponent

fdescribe(`GameActionsComponent`, () => {
  beforeEach(() => {
    component = new GameActionsComponent()
  })

  it(`should have correct initial values`, () => {
    expect(component.isExecuteButtonDisabled).toEqual(true)
    expect(component.isLoadNextButtonDisabled).toEqual(true)
    expect(component.isRetryLevelButtonDisabled).toEqual(true)
  })
})
