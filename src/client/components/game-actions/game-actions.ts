import { Component, Vue, Watch } from 'vue-property-decorator'
import { Store, GameStateStatus } from '../../store'
import { IBunnyGameState, BunnyGameStatus } from '../../bunny-game'
import logger from '../../util/logger'

// TODO 23.12.2017 nviik - Add specs for this component
@Component({
  template: require('./game-actions.html'),
})
export class GameActionsComponent extends Vue {
  public isExecuteButtonDisabled: boolean = true
  public isRetryLevelButtonDisabled: boolean = true
  public isLoadNextButtonDisabled: boolean = true

  @Watch('$store.state.game.bunnyGameState')
  private onCurrentCommandChanged(state: IBunnyGameState): void {
    this.isExecuteButtonDisabled = state.status !== BunnyGameStatus.IDLE
    this.isRetryLevelButtonDisabled = state.status === BunnyGameStatus.GAME_OVER
    this.isLoadNextButtonDisabled = state.status === BunnyGameStatus.WIN
  }

  public executeCode(): void {
    logger.info('execute code')
  }

  public retryLevel(): void {
    logger.info('retry level')
  }

  public loadNextLevel(): void {
    logger.info('load next level')
  }

  public changeGameSpeed(): void {
    logger.info('change game speed')
  }
}
