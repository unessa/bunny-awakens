import { Component, Vue, Watch } from 'vue-property-decorator'
import { Store, IRootState, GameStateStatus } from '../../store'
import { parseScriptToCommands } from '../../game-code'
import { BunnyGame, IBunnyGameState, BunnyGameStatus } from '../../bunny-game'
import logger from '../../util/logger'

const GAME_ELEMENT_ID = 'phaser-game'

@Component({
  template: require('./game-view.html')
})
export class GameViewComponent extends Vue {
  private bunnyGame: BunnyGame

  public numberOfCarrotsCollected: number = 0
  public totalNumberOfCarrots: number = 0
  public status: string = 'IDLE'
  public isNextLevelButtonDisabled: boolean = true

  mounted(): void {
    logger.info('GameViewComponent:mounted - create Game')
    this.bunnyGame = new BunnyGame(GAME_ELEMENT_ID, 320, 320, this.onBunnyGameStateChanged)
  }

  @Watch('$store.state.game.status')
  private onGameStateStatusChanged(status: GameStateStatus): void {
    if (status === GameStateStatus.RUNNING) {
      const commands = parseScriptToCommands(Store.getters.getCurrentGameCode)
      this.bunnyGame.setAndRunGameCode(commands)
    }
  }

  private onBunnyGameStateChanged(state: IBunnyGameState): void {
    logger.info('GameViewComponent::onBunnyGameStateChanged', state)

    if (state.status === BunnyGameStatus.RUNNING && state.currentExecutingCommand) {
      this.isNextLevelButtonDisabled = true
      Store.commit('setCurrentCommand', state.currentExecutingCommand)
    } else if (state.status === BunnyGameStatus.WIN) {
      this.isNextLevelButtonDisabled = false
    }

    this.numberOfCarrotsCollected = state.carrotsCollected
    this.totalNumberOfCarrots = state.totalCarrots
    this.status = state.status
  }

  public changeSpeed(): void {
    logger.info('Change execution speed!')
    this.bunnyGame.changeSpeed()
  }

  public loadNextLevel(): void {
    logger.info('Load next level')
  }
}
