import { Component, Vue, Watch } from 'vue-property-decorator'
import { Store } from '../../store'
import logger from '../../util/logger'

@Component({
  template: require('./navbar.html'),
  components: {
  }
})
export class NavbarComponent extends Vue {
  environment: string | undefined = process.env.ENV

  @Watch('$route.path')
  pathChanged() {
    logger.info('Changed current path to', this.$route.path)
  }

  mounted() {
    logger.info('navbar mounted')
  }

  setLocale(locale: string) {
    Store.commit('setLocale', locale)
  }
}
