import * as esprima from 'esprima'
import { CallExpression, Expression, Identifier, ExpressionStatement } from 'estree'

import {
  parseScriptToCommands,
  parseExpressionStatement,
  INVALID_LOCATION
} from './evaluate-code'

describe(`Evaluate code`, () => {
  describe(`parseExpressionStatement`, () => {
    it(`should create callstack with functions`, () => {
      const callstack = parseScriptToCommands(`moveLeft(); moveRight();\nmoveUp();`)
      expect(callstack).toEqual([
        {
          name: 'moveLeft',
          arguments: [],
          location: {
            start:  { line: 1, column: 0 },
            end: { line: 1, column: 10 }
          }
        },
        {
          name: 'moveRight',
          arguments: [],
          location: {
            start: { line: 1, column: 12 },
            end: { line: 1, column: 23 }
          }
        },
        {
          name: 'moveUp',
          arguments: [],
          location: {
            start: { line: 2, column: 0 },
            end: { line: 2, column: 8 }
          }
        }
      ])
    })
  })

  describe(`parseExpressionStatement`, () => {
    it(`should parse GameEngineCommand without any parameters given`, () => {
      const parsed = esprima.parseScript(`moveLeft()`)
      const expressionStatement = parsed.body[0] as ExpressionStatement
      const command = parseExpressionStatement(expressionStatement.expression)
      expect(command).toEqual({
        name: 'moveLeft',
        arguments: [],
        location: {
          start: INVALID_LOCATION,
          end: INVALID_LOCATION
        }
      })
    })

    it(`should throw error if expression type is not CallExpression`, () => {
      const parsed = esprima.parseScript(`[]`)
      const expressionStatement = parsed.body[0] as ExpressionStatement
      const throws = () => parseExpressionStatement(expressionStatement.expression)
      expect(throws).toThrowError(`Unsupported expression type: ArrayExpression`)
    })

    it(`should throw error if command is undefined`, () => {
      const parsed = esprima.parseScript(`undefinedFunction()`)
      const expressionStatement = parsed.body[0] as ExpressionStatement
      const throws = () => parseExpressionStatement(expressionStatement.expression)
      expect(throws).toThrowError(`Command name not found: undefinedFunction`)
    })
  })
})
