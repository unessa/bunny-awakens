import * as estree from 'estree'
import * as esprima from 'esprima'
import logger from '../util/logger'
import { AVAILABLE_COMMANDS, IGameEngineCommand } from './game-engine-api'

export const INVALID_LOCATION = { line: -1, column: -1 }

export function parseScriptToCommands(
  program: string,
): IGameEngineCommand[] {
  const res = esprima.parseScript(program, { loc: true })
  const callStack: IGameEngineCommand[] = []

  res.body.forEach(item => {
    try {
      switch (item.type) {
        case 'ExpressionStatement':
          callStack.push(parseExpressionStatement(item.expression))
          break

        default:
          throw new Error(`Unsupported statement: ${item.type}`)
      }
    } catch (e) {
      logger.error(`Error while parsing commands: ${e}`)
    }
  })

  return callStack
}

export function parseExpressionStatement(expression: estree.Expression): IGameEngineCommand {
  if (expression.type !== 'CallExpression') {
    throw new Error(`Unsupported expression type: ${expression.type}`)
  }

  const callExpression = expression as estree.CallExpression
  const expressionName = (expression.callee as estree.Identifier).name

  if (!AVAILABLE_COMMANDS.has(expressionName)) {
    throw new Error(`Command name not found: ${expressionName}`)
  }

  return {
    name: expressionName,
    arguments: callExpression.arguments,
    location: {
      start: callExpression.loc && callExpression.loc.start || INVALID_LOCATION,
      end: callExpression.loc && callExpression.loc.end || INVALID_LOCATION
    }
  }
}
