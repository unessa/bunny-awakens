import * as estree from 'estree'

export interface ISourceLocation {
  start: estree.Position,
  end: estree.Position
}

export interface IGameEngineCommand {
  name: string
  arguments: any[],
  location: ISourceLocation
}

export const COMMAND_MOVE_LEFT = 'moveLeft'
export const COMMAND_MOVE_RIGHT = 'moveRight'
export const COMMAND_MOVE_UP = 'moveUp'
export const COMMAND_MOVE_DOWN = 'moveDown'

export const AVAILABLE_COMMANDS = new Set([
  COMMAND_MOVE_LEFT,
  COMMAND_MOVE_RIGHT,
  COMMAND_MOVE_UP,
  COMMAND_MOVE_DOWN
])
