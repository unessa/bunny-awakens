export { parseScriptToCommands, INVALID_LOCATION } from './evaluate-code'

export {
  IGameEngineCommand,
  ISourceLocation,
  COMMAND_MOVE_LEFT,
  COMMAND_MOVE_RIGHT,
  COMMAND_MOVE_UP,
  COMMAND_MOVE_DOWN
} from './game-engine-api'
