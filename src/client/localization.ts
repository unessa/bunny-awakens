import Vue from 'vue'
import vuexI18n from 'vuex-i18n'
import { Store } from './store'

export async function setupLocalization(): Promise<void> {
  const currentLocale = Store.state.user.locale
  const res = await fetch(`assets/i18n/${currentLocale}.json`)
  const translations = await res.text()

  Vue.use(vuexI18n.plugin, Store)
  Vue.i18n.add(currentLocale, JSON.parse(translations))
  Vue.i18n.set(currentLocale)
}
