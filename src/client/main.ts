import Vue from 'vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue/es'
import { createRouter } from './router'
import { Store } from './store'
import { AppPage } from './pages/app/app'
import { setupLocalization } from './localization'

import './main.scss'

if (process.env.NODE_ENV === 'production') {
  // tslint:disable-next-line
  require('./pwa')
}

Vue.use(BootstrapVue)

setupLocalization().then(() => {
  const main = new Vue({
    el: '#app-main',
    router: createRouter(),
    store: Store,
    render: h => h(AppPage)
  })
})
