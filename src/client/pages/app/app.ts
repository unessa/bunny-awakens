import { Component, Vue, Watch } from 'vue-property-decorator'
import { NavbarComponent } from '../../components/navbar'
import { Store } from '../../store'

@Component({
  template: require('./app.html'),
  components: {
    navbarComponent: NavbarComponent
  }
})
export class AppPage extends Vue {
  @Watch('$store.state.user.locale')
  onLocaleChanged() {
    // TODO 10.12.2017 nviik - Figure out how to reload component without full page reload
    location.reload()
  }
}
