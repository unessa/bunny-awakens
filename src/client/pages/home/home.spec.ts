// 08.12.2017 nviik - Disable UI test for now. Will need to decide what to do with these

// import { HomePage } from './home'
// import { ComponentTest } from '../../util/component-test'

// describe('Home component', () => {
//   let directiveTest: ComponentTest

//   beforeEach(() => {
//     directiveTest = new ComponentTest('<div><home></home></div>', {
//       home: HomePage
//     })
//   })

//   it('should render correct contents', async () => {
//     directiveTest.createComponent()
//     await directiveTest.execute(vm => {
//       const title = vm.$el.querySelector('.jumbotron .display-3')
//       const lead = vm.$el.querySelector('.jumbotron .lead')

//       if (title) {
//         expect(title.textContent).toEqual(`Koodikoulu`)
//       }
//       if (lead) {
//         expect(lead.textContent).toEqual(`Bunny Awakens`)
//       }
//     })
//   })
// })
