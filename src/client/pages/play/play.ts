import { Component, Vue } from 'vue-property-decorator'
import { CodeEditorComponent } from '../../components/code-editor'
import { GameViewComponent } from '../../components/game-view'
import { GameActionsComponent } from '../../components/game-actions'

@Component({
  template: require('./play.html'),
  components: {
    codeEditor: CodeEditorComponent,
    gameView: GameViewComponent,
    gameActions: GameActionsComponent
  }
})
export class PlayPage extends Vue {
}
