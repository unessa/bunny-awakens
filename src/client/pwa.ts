import * as runtime from 'offline-plugin/runtime'
import logger from './util/logger'

runtime.install({
  onInstalled() {
    logger.info('PWA installed')
  },

  onUpdating() {
    logger.info('PWA updating resources')
  },

  onUpdateReady() {
    logger.info('PWA resource update completed')
    runtime.applyUpdate()
  },

  onUpdateFailed() {
    logger.info('PWA resource update failed')
  },

  onUpdated() {
    logger.info('PWA resource updated')
    location.reload()
  }
})
