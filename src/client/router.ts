import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import { HomePage } from './pages/home'
import { PlayPage } from './pages/play'

Vue.use(VueRouter)

function createRoutes(): RouteConfig[] {
  return [
    {
      path: '/',
      component: HomePage
    },
    {
      path: '/play',
      component: PlayPage
    }
  ]
}

export function createRouter(): VueRouter {
  return new VueRouter({
    mode: 'history',
    routes: createRoutes()
  })
}
