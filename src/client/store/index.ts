import Vue from 'vue'
import Vuex, { Payload } from 'vuex'
import VuexPersistence from 'vuex-persist'
import { UserState, IUserState } from './state/user-state'
import { GameState, IGameState } from './state/game-state'

export { GameStateStatus } from './state/game-state'

export interface IRootState {
  user: IUserState,
  game: IGameState
}

Vue.use(Vuex)

const vuexLocal = new VuexPersistence<IRootState, Payload>({
  storage: window.localStorage,
  reducer: state => Object.assign({}, state, { i18n: undefined })
})

export const Store = new Vuex.Store({
  modules: {
    user: UserState,
    game: GameState
  },
  plugins: [
    // FIXME 16.12.2017 nviik - Enable this when state handling is fixed
    // vuexLocal.plugin
  ]
})
