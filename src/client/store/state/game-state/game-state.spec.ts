import Vue from 'vue'
import Vuex, { Store } from 'vuex'

import {
  IGameEngineCommand,
  COMMAND_MOVE_LEFT
} from '../../../game-code'

import {
  GameState,
  createInitialGameState,
  GameStateStatus,
  IGameState
} from './game-state'

Vue.use(Vuex)

const TEST_GAME_CODE = `${COMMAND_MOVE_LEFT}();`
const TEST_GAME_COMMAND: IGameEngineCommand = {
  name: COMMAND_MOVE_LEFT,
  arguments: [],
  location: {
    start: { line: 1, column: 0 },
    end: { line: 1, column: 5 }
  }
}

let MockStore: Store<any>

describe(`GameState (Store)`, () => {
  beforeAll(() => {
    MockStore = new Vuex.Store({
      state: createInitialGameState(),
      mutations: GameState.mutations,
      getters: GameState.getters,
      actions: GameState.actions
    })
  })

  it(`should have correct initial values`, () => {
    expect(MockStore.state).toEqual({
      status: GameStateStatus.IDLE,
      currentCode: '',
      currentCommand: null
    })
  })

  describe(`mutations and getters`, () => {
    testSetterGetter('status', 'setStatus', 'getStatus', GameStateStatus.ERROR)
    testSetterGetter('currentCode', 'setCurrentGameCode', 'getCurrentGameCode', TEST_GAME_CODE)
    testSetterGetter('currentCommand', 'setCurrentCommand', 'getCurrentCommand', TEST_GAME_COMMAND)
  })

  describe(`actions`, () => {
    describe(`when executeCode is dispatched`, () => {
      it(`should reset current state when new code is set`, () => {
        MockStore.commit('setStatus', GameStateStatus.IDLE)
        MockStore.dispatch('executeCode', TEST_GAME_CODE)
        expect(MockStore.state).toEqual({
          status: GameStateStatus.RUNNING,
          currentCode: TEST_GAME_CODE,
          currentCommand: null
        })
      })

      it(`should throw error if status is not idle`, () => {
        const throws = () => {
          MockStore.commit('setStatus', GameStateStatus.RUNNING)
          MockStore.dispatch('executeCode', TEST_GAME_CODE)
        }
        expect(throws).toThrowError(`Unable to execute code when GameState status is ${GameStateStatus.RUNNING}`)
      })
    })
  })
})

// TODO 18.12.2017 nviik - Move this to store helper functions
function testSetterGetter(
  propertyName: string,
  setterFn: string,
  getterFn: string,
  mockValue: any
): void {
  describe(`"${propertyName}"`, () => {
    it(`should not to have expected value`, () => {
      expect(MockStore.state[propertyName]).not.toEqual(mockValue)
    })

    it(`should do commit`, () => {
      MockStore.commit(setterFn, mockValue)
      expect(MockStore.state[propertyName]).toEqual(mockValue)
    })

    it(`should get current value`, () => {
      expect(MockStore.getters[getterFn]).toEqual(mockValue)
    })
  })
}
