import { Module, MutationTree, GetterTree, ActionTree, ActionContext } from 'vuex'
import logger from '../../../util/logger'
import { IGameEngineCommand } from '../../../game-code'

export enum GameStateStatus {
  IDLE = 'IDLE',
  RUNNING = 'RUNNING',
  ERROR = 'ERROR',
  FAIL = 'FAIL',
  WIN = 'WIN'
}

export interface IGameState {
  currentCode: string,
  status: GameStateStatus,
  currentCommand: IGameEngineCommand | null
}

export function createInitialGameState(): IGameState {
  return {
    status: GameStateStatus.IDLE,
    currentCode: '',
    currentCommand: null
  }
}

const mutations: MutationTree<IGameState> = {
  setCurrentGameCode(state: IGameState, code: string): void {
    state.currentCode = code
  },
  setStatus(state: IGameState, status: GameStateStatus): void {
    state.status = status
  },
  setCurrentCommand(state: IGameState, command: IGameEngineCommand): void {
    state.currentCommand = command
  }
}

const getters: GetterTree<IGameState, any> = {
  getCurrentGameCode(state: IGameState): string {
    return state.currentCode
  },
  getStatus(state: IGameState): GameStateStatus {
    return state.status
  },
  getCurrentCommand(state: IGameState): IGameEngineCommand | null {
    return state.currentCommand
  }
}

const actions: ActionTree<IGameState, any> = {
  executeCode(context: ActionContext<IGameState, any>, code: string): void {
    if (context.getters.getStatus === GameStateStatus.IDLE) {
      context.commit('setCurrentGameCode', code)
      context.commit('setStatus', GameStateStatus.RUNNING)
      context.commit('setCurrentCommand', null)
    } else {
      throw new Error(`Unable to execute code when GameState status is ${context.getters.getStatus}`)
    }
  }
}

export const GameState: Module<IGameState, any> = {
  state: createInitialGameState(),
  mutations,
  getters,
  actions
}
