import Vue from 'vue'
import Vuex, { Store } from 'vuex'

import {
  UserState,
  createInitialUserState,
  LanguageCode,
  IUserState
} from './user-state'

Vue.use(Vuex)

const MOCK_DISPLAY_NAME = 'Bunnier'
let MockStore: Store<any>

describe(`GameState (store)`, () => {
  beforeAll(() => {
    MockStore = new Vuex.Store({
      state: createInitialUserState(),
      mutations: UserState.mutations,
      getters: UserState.getters,
      actions: UserState.actions
    })
  })

  it(`should have correct initial values`, () => {
    expect(MockStore.state).toEqual({
      locale: LanguageCode.EN,
      displayName: ''
    })
  })

  describe(`mutations and getters`, () => {
    testSetterGetter('locale', 'setLocale', 'getLocale', LanguageCode.FI)
    testSetterGetter('displayName', 'setDisplayName', 'getDisplayName', MOCK_DISPLAY_NAME)
  })
})

// TODO 18.12.2017 nviik - Move this to store helper functions
function testSetterGetter(
  propertyName: string,
  setterFn: string,
  getterFn: string,
  mockValue: any
): void {
  describe(`"${propertyName}"`, () => {
    it(`should not to have expected value`, () => {
      expect(MockStore.state[propertyName]).not.toEqual(mockValue)
    })

    it(`should do commit`, () => {
      MockStore.commit(setterFn, mockValue)
      expect(MockStore.state[propertyName]).toEqual(mockValue)
    })

    it(`should get current value`, () => {
      expect(MockStore.getters[getterFn]).toEqual(mockValue)
    })
  })
}
