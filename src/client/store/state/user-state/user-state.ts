import { Module, MutationTree, GetterTree } from 'vuex'

export enum LanguageCode {
  EN = 'en',
  FI = 'fi'
}

export interface IUserState {
  locale: LanguageCode,
  displayName: string
}

export function createInitialUserState(): IUserState {
  return {
    locale: LanguageCode.EN,
    displayName: ''
  }
}

const mutations: MutationTree<IUserState> = {
  setLocale(state: IUserState, locale: LanguageCode): void {
    state.locale = locale
  },
  setDisplayName(state: IUserState, displayName: string): void {
    state.displayName = displayName
  }
}

const getters: GetterTree<IUserState, any> = {
  getLocale(state: IUserState): LanguageCode {
    return state.locale
  },
  getDisplayName(state: IUserState): string {
    return state.displayName
  }
}

export const UserState: Module<IUserState, any> = {
  state: createInitialUserState(),
  mutations,
  getters
}
