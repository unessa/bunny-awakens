import 'pixi'
import 'p2'
import * as Phaser from 'phaser-ce'
import Vue, { Component } from 'vue'
import BootstrapVue from 'bootstrap-vue/es'

export interface IComponents {
  [key: string]: Component
}

export class ComponentTest {
  public vm: Vue

  constructor(private template: string, private components: IComponents) {
  }

  public createComponent(createOptions?: any): void {
    const defaultOptions = {
      template: this.template,
      components: this.components
    }

    Vue.use(BootstrapVue)

    const options = { ...defaultOptions, createOptions }
    this.vm = new Vue(options).$mount()
  }

  public async execute(
    callback: (vm: Vue) => Promise<void> | void
  ): Promise<void> {
    await Vue.nextTick()
    await callback(this.vm)
  }
}
