function info(message: string, ...args: any[]): void {
  if (process.env.ENV === 'development') {
    // tslint:disable-next-line
    console.log(message, ...args)
  }
}

function warning(message: string, ...args: any[]): void {
  // tslint:disable-next-line
  console.warn(`${message}`, ...args)
}

function error(message: string, ...args: any[]): void {
  // tslint:disable-next-line
  console.error(`${message}`, ...args)
}

export default {
  info,
  warning,
  error
}
