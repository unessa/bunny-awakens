import * as path from 'path'
import * as http from 'http'
import * as express from 'express'
import * as history from 'connect-history-api-fallback'

const DEFAULT_PORT = 3000
const INDEX_HTML = path.join(__dirname, '../../dist/index.html')
const PUBLIC_ASSETS = path.join(__dirname, '../../dist')

async function createServer(): Promise<http.Server> {
  const app = express()

  app.use(history())
  app.use(express.static(PUBLIC_ASSETS))
  app.get('*', (req: express.Request, res: express.Response) => {
    res.sendFile(INDEX_HTML)
    res.end()
  })

  const port = process.env.PORT || DEFAULT_PORT
  const server = app.listen(port)
  return server
}

createServer().then(instance => {
  process.on('SIGINT', () => {
    instance.close()
    process.exit(0)
  })
})
